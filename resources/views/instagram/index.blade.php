@extends('welcome')

@section('header')
    <div class="header">
        <a href="{{action('InstagramController@rules')}}" class="header--button header--button__rules">
            <span class="header--buttontext">Taisyklės</span>
            <em class="header--buttonem"></em>
        </a>
    </div>
    <div class="app--split">
        <h1 class="app--title">GALERIJA</h1>
        <p class="app--subtitle">Balsuok už labiausiai pašėlusį nuotykį!</p>
        <span class="app--uploadbutton" id="uploadClick">
            <span class="app--uploadtext">Įkelti</span>
        </span>
        <span class="app--uploadbutton" id="fb-login-button" style="display: block">
            <span class="app--uploadtext">Prisijungti</span>
        </span>
    </div>
@stop

@section('content')
    <div class="overlay"></div>
    <div class="modal">
        <div id="modal_step1" class="modal--container cf">
            <div class="modal--entry modal--entry__instagram">
                <a class="modal--link__instagram" href="instagram://user?username=untitled.tiff"></a>
            </div>
            <div class="modal--entry modal--entry__facebook" style="border-right:0;">
                {!! Form::open(array('url'=>'upload-file','files'=>true)) !!}
                <div class="imageInputContainer">
                    <img src="" id="blah"/>
                    <div id="blah_overlay"></div>
                    <input class="uploadImage" type="file" name="file"
                           onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0]);$('#blah').show();$('#blah_overlay').show();setTimeout(function(){$('.continueUpload').click();},1);">
                </div>
                <input id="fbIdForUpload" name="fb_id" type="hidden" value="0"/>
                {!! Form::submit('Toliau',['class'=>'continueUpload']) !!}
                {!! Form::close() !!}
            </div>
            <div class="modal--choose">
                <span class="modal--mobTitle">Įkelti nuotrauka:</span>
                <div class="modal--choise modal--choise__instagram"></div>
                <div class="modal--choise modal--choise__facebook"></div>
            </div>
            <div class="back"></div>
        </div>
    </div>
    @if($filename)
        <div class="overlay" style="display:block"></div>
        <div class="modal" style="display:block">
            <div class="modal--container">
                <div class="cropModal">
                    <div>
                        <img id="image" src="upload/{{$filename}}"/>
                    </div>
                </div>
                <span class="app--uploadbutton" id="croppImage" style="top: 106%;right: auto;left:50%;margin-left:-150px;">
                    <span class="app--uploadtext">Įkelti</span>
                </span>
            </div>
        </div>
    @endif
    <div class="app--content">
        <div class="imagesContainer cf">
            @foreach($instagram_photos as $instagram_photo)
                <div class="imagesContainer--image img" data-src="{{ $instagram_photo->url }}" data-entry-id="{{ $instagram_photo->id }}">
                    <div class="imagesContainer--overlay imagesContainer--overlay__hidden">
                        <span class="imagesContainer--overlaycount counter">
                            {{ $instagram_photo->likes }}
                        </span>

                        <span class="imagesContainer--overlaytext">JŪSŲ BALSAS UŽSKAITYTAS</span>
                        <div class="imagesContainer--overlaycountlike"></div>
                    </div>
                    <div class="imagesContainer--like" data-id="{{ $instagram_photo->instagram_id }}">
                        <span class="imagesContainer--count counter">{{ $instagram_photo->likes }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop