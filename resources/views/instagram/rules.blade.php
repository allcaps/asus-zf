@extends('welcome')

@section('header')
    <div class="header">
        <a href="{{action('InstagramController@index')}}" class="header--button header--button__gallery">
            <span class="header--buttontext">Galerija</span>
            <em class="header--buttonem"></em>
        </a>
    </div>
    <div class="app--split">
        <h1 class="app--title">Taisyklės</h1>
        <p class="app--subtitle">Kaip dalyvauti žaidime?</p>
    </div>
@stop

@section('content')
    <div class="app--content app--content__rules cf">
        <div class="app--rulesEntry app--rulesEntry__rule1"></div>
        <div class="app--rulesEntry app--rulesEntry__rule2"></div>
        <div class="app--rulesEntry app--rulesEntry__rule3"></div>
        <div class="app--rulesEntry app--rulesEntry__rule4"></div>
        <div class="app--winnerText">
            NUGALĖTOJĄ PASKELBSIME BIRŽELIO 11d.<br/>
            ASUS LIETUVA FACEBOOK PASKYROJE
        </div>
    </div>
@stop