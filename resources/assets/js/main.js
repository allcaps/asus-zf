!function(t){"use strict";var e=t.HTMLCanvasElement&&t.HTMLCanvasElement.prototype,o=t.Blob&&function(){try{return Boolean(new Blob)}catch(t){return!1}}(),n=o&&t.Uint8Array&&function(){try{return 100===new Blob([new Uint8Array(100)]).size}catch(t){return!1}}(),r=t.BlobBuilder||t.WebKitBlobBuilder||t.MozBlobBuilder||t.MSBlobBuilder,a=/^data:((.*?)(;charset=.*?)?)(;base64)?,/,i=(o||r)&&t.atob&&t.ArrayBuffer&&t.Uint8Array&&function(t){var e,i,l,u,b,c,d,B,f;if(e=t.match(a),!e)throw new Error("invalid data URI");for(i=e[2]?e[1]:"text/plain"+(e[3]||";charset=US-ASCII"),l=!!e[4],u=t.slice(e[0].length),b=l?atob(u):decodeURIComponent(u),c=new ArrayBuffer(b.length),d=new Uint8Array(c),B=0;B<b.length;B+=1)d[B]=b.charCodeAt(B);return o?new Blob([n?d:c],{type:i}):(f=new r,f.append(c),f.getBlob(i))};t.HTMLCanvasElement&&!e.toBlob&&(e.mozGetAsFile?e.toBlob=function(t,o,n){t(n&&e.toDataURL&&i?i(this.toDataURL(o,n)):this.mozGetAsFile("blob",o))}:e.toDataURL&&i&&(e.toBlob=function(t,e,o){t(i(this.toDataURL(e,o)))})),"function"==typeof define&&define.amd?define(function(){return i}):"object"==typeof module&&module.exports?module.exports=i:t.dataURLtoBlob=i}(window);
//# sourceMappingURL=canvas-to-blob.min.js.map
$(document).ready(function(){
	setTimeout(function(){
		FB.getLoginStatus(function(response) {
			if (response.status == 'connected') {
				window.fbLoggedIn = true;
				window.fbUser = response;
				$('#fb-login-button').hide();
				$('#uploadClick').show();
				getVoted();
			}
		});
	},1000);

	if(window.innerWidth < 800){
		$('.modal--entry').hide();

		$('.modal--choose').show();
		window.mobile = true;
	}

	$('#fb-login-button').on('click',getLoginStatus);
	$("div.img").unveil(1000);

	$('.imagesContainer--like').on('click',function(){
		window.voteId = $(this).attr('data-id');
		getLoginStatus();
	});

	$("#uploadClick").click(function(){
		$(".overlay").fadeIn('fast');
		$(".modal").fadeIn('fast');
		document.getElementById("fbIdForUpload").value = window.fbUser.id;
	});

	$(".overlay").click(function(){
		$(".overlay").fadeOut('fast');
		$(".modal").fadeOut('fast');
		if(window.mobile) {
			$(".modal--choose").show();
			$('.modal--entry__facebook').hide();
			$('.modal--entry__instagram').hide();
		}
	});
	if($('.cropModal').is(":visible")){
		initCropp();
	}
	$("#croppImage").click(function(){
		submitCropp();
	});
	if(window.mobile){
		$(".modal--choise__instagram").click(function(){
			$(".modal--choose").fadeOut('fast');
			$('.modal--entry__instagram').fadeIn('fast');
			$('.modal .back').show();
		});
		$(".modal--choise__facebook").click(function(){
			$(".modal--choose").fadeOut('fast');
			$('.modal--entry__facebook').fadeIn('fast');
			$('.modal .back').show();
		});
		$('.modal .back').click(function () {
			$('.modal--entry__facebook').fadeOut('fast');
			$('.modal--entry__instagram').fadeOut('fast');
			$(".modal--choose").fadeIn('fast');
			$('.modal .back').hide();
		})
	}
});

var ajar_url = 'https://zaidimas.zenfone3.lt/';

window.mobile = false;
window.fbLoggedIn = false;
window.fbUser = false;
window.voteId = false;

function getVoted() {
	FB.api('/me', function (response) {
		window.fbLoggedIn = true;
		window.fbUser = response;
		$.ajax({
			url: ajar_url + 'get-voted',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			type: 'POST',
			dataType: 'json',
			data: {instagram_id: window.voteId, user_id: window.fbUser.id},
			success: function (result) {
				for (var i = 0; i < result.length; i++) {
					$('.imagesContainer--image[data-entry-id="' + result[i].instagram_photo_id + '"]').find('.imagesContainer--overlay').removeClass('imagesContainer--overlay__hidden');
					$('.imagesContainer--image[data-entry-id="' + result[i].instagram_photo_id + '"]').find('.imagesContainer--like').addClass('imagesContainer--like__hidden');
				}
			}
		});
	});
}

function getLoginStatus(){
	if(!window.fbLoggedIn) {
		FB.getLoginStatus(function (response) {
			if (response.status == 'connected') {
				window.fbLoggedIn = true;
				window.fbUser = response;
				$('#fb-login-button').hide();
			} else if (response.status === 'not_authorized') {
				fbLogin();
			} else {
				fbLogin();
			}
		});
	}else{
		FB.api('/me', function (response) {
			window.fbLoggedIn = true;
			window.fbUser = response;
			$.ajax({
				url: ajar_url + 'vote',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: 'POST',
				dataType: 'json',
				data: {instagram_id: window.voteId, user_id: window.fbUser.id},
				success: function(result){
					if( "undefined" !== typeof result.success ){
						if(result.success){
							$('.imagesContainer--like[data-id="'+window.voteId+'"]').parent().find('.counter').each(function(){
								$(this).text(parseInt($(this).text()) + 1);
							});
							$('.imagesContainer--like[data-id="'+window.voteId+'"]').parent().find('.imagesContainer--overlay').removeClass('imagesContainer--overlay__hidden');
							$('.imagesContainer--like[data-id="'+window.voteId+'"]').addClass('imagesContainer--like__hidden');
						}else{

						}
					}
				}
			});
		});
	}
}

function fbLogin(){
	FB.login(function(response) {
		if (response.authResponse) {
			FB.api('/me', function (response) {
				window.fbLoggedIn = true;
				window.fbUser = response;
				$('#fb-login-button').hide();
			});
		}
	});
}

window.cropper = false;
function initCropp(){
	var image = document.getElementById('image');
	window.cropper = new Cropper(image, {
		aspectRatio: 1,
		ready: function () {
			window.cropper2 = this.cropper;
		}
	});
}

function submitCropp() {
	if(window.cropper.getCroppedCanvas().toBlob) {
		window.cropper.getCroppedCanvas().toBlob(function (blob) {
			var formData = new FormData();

			formData.append('cropped_image', blob);
			formData.append('fb_id', window.fbUser.id);

			// Use `jQuery.ajax` method
			$.ajax('/upload-file', {
				method: "POST",
				data: formData,
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				processData: false,
				contentType: false,
				success: function () {
					window.location.href = ajar_url;
				},
				error: function () {
					console.log('Upload error');
				}
			});
		},'image/jpeg');
	}
}