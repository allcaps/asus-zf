<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstagramVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instagram_photo_id')->index('instagram_photo_id');
            $table->string('user_id',40)->index('user_id');
            $table->ipAddress('ip_addess');
            $table->string('user_agent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_votes');
    }
}
